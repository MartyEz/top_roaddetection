import scala.collection.mutable.MutableList
  // Import the API library
import com.tncy.top.image.ImageWrapper;
object road extends App {
  
  
  val skerx=Array(Array(-1,0,1),Array(-2,0,2),Array(-1,0,1));
  val skery=Array(Array(-1,-2,-1),Array(0,0,0),Array(1,2,1));
  val moy1=Array(Array(1,1,1),Array(1,2,1),Array(1,1,1));
  val lapl=Array(Array(0,1,0),Array(1,-4,1),Array(0,1,0));
  val laplD=Array(Array(1,1,1),Array(1,-8,1),Array(1,1,1));
  
  val chemin="C:\\Users\\Lucas\\Desktop\\PROJET\\Images\\Images\\ImagesTests\\"
val file="4.jpg"
val num="4"

var fileName : String = chemin+file;
var wrappedImage : ImageWrapper = new ImageWrapper(fileName);
var image2D : Array[Array[Int]] = wrappedImage.getImage();
var save2d=copy(image2D)
//affiche2D(save2d)

  def GreyLevel(src:Array[Array[Int]]){
    var height=src.length;
  var width=src(0).length
 
   for (row <- 0 to height-1) {
     for (col <-0 to width-1) {

       var str =String.format("%" + 8 + "s", Integer.toHexString(src(row)(col))).replace(' ', '0'); // conversion int en hexa sous forme d'un string
       var r = Integer.parseInt(str.drop(2).take(2), 16); // on extrait les couleurs de la chaine hexa. ici rouge
       var g = Integer.parseInt(str.drop(4).take(2), 16); // vert
       var b = Integer.parseInt(str.drop(6).take(2), 16); // bleu
       var gris = 0.2126*r+ 0.7152*g+0.0722*b; // on calcul le niveau de gris
       //var gris = (r+g+b)/3; // on calcul le niveau de gris
       var rt = gris.toInt
       src(row)(col)=rt; // On modifie la valeur du pixel avec le nouvel entier.
     }
   }
}
  
def copy(src:Array[Array[Int]]):Array[Array[Int]]={
  var height=src.length;
  var width=src(0).length
  var dst:Array[Array[Int]]=Array.ofDim[Int](height,width);
  for(i <- 0 to height-1){
    for(j <-0 to width-1){
      dst(i)(j)=src(i)(j)   
    }
  }
  return dst 
}
 
def convol(src:Array[Array[Int]],ker:Array[Array[Int]]):Array[Array[Int]]={
  
  var height=src.length;
  var width=src(0).length
  var outa=Array.ofDim[Int](height,width);
  
  for(i<-1 to height-2){
    for(j<-1 to width-2){
      outa(i)(j)=src(i)(j)*ker(1)(1)+src(i-1)(j)*ker(0)(1)+src(i+1)(j)*ker(2)(1)+src(i)(j-1)*ker(1)(0)+src(i+1)(j+1)*ker(2)(2)+src(i-1)(j-1)*ker(0)(0)+src(i)(j+1)*ker(1)(2)+ker(0)(2)*src(i-1)(j+1)+ker(2)(0)*src(i+1)(j-1)
      //outa(i)(j)=outa(i)(j)/4
      }
    }
    return outa
  }
  
 def affiche2D(src:Array[Array[Int]]){
   var height=src.length;
   var width=src(0).length; 
   for(i<-0 to height-1){
     for(j<-0 to width-1){
       print(src(i)(j)+"-")
       if(j==width-1)
         println()
     }
   }
 }

 def sobel(src:Array[Array[Int]]):Array[Array[Int]]={
   var gx=convol(src,skery)
   var gy=convol(src,skerx)
   var height=src.length;
   var width=src(0).length
   var dst=Array.ofDim[Int](height,width);
   for(i<-1 to height-2){
      for(j<-1 to width-2){
        src(i)(j)=(Math.sqrt(gx(i)(j)*gx(i)(j)+gy(i)(j)*gy(i)(j))/4).toInt;
      }
   }
 return dst
 }
 
  def moy(src:Array[Array[Int]],coeff:Int):Array[Array[Int]]={
   var gx=convol(src,moy1)
   
   var height=src.length;
   var width=src(0).length
   var dst=Array.ofDim[Int](height,width);
   for(i<-1 to height-2){
      for(j<-1 to width-2){
        src(i)(j)=gx(i)(j)/coeff.toInt;
      }
   }
 return dst
 }
  
 def laplace(src:Array[Array[Int]]):Array[Array[Int]]={
   var gx=convol(src,laplD)
   
   var height=src.length;
   var width=src(0).length
   var dst=Array.ofDim[Int](height,width);
   for(i<-1 to height-2){
      for(j<-1 to width-2){
        src(i)(j)=gx(i)(j).toInt;
      }
   }
 return dst
 }
 
 
  def rehauss(src:Array[Array[Int]]):Array[Array[Int]]={
   
   var height=src.length;
   var width=src(0).length
   var dst=Array.ofDim[Int](height,width);
   for(i<-1 to height-2){
      for(j<-1 to width-2){
        src(i)(j)=src(i)(j)*5.toInt;
      }
   }
 return dst
 }
 
  
  def reduce(src:Array[Array[Int]]){
        var height=src.length;
    var width=src(0).length
    
    var moy=0
    for(i<-1 to height-2){
      for(j<-1 to width-2){
        moy=(src(i-1)(j-1)+src(i)(j-1)+src(i+1)(j-1)+src(i-1)(j)+src(i+1)(j)+src(i-1)(j+1)+src(i)(j+1)+src(i+1)(j+1))/8
        if(src(i)(j)>=4*moy)
          src(i)(j)=0
      }
    }
    
  }
  
  
  def moyenne(src:Array[Array[Int]],centerx:Int,centery:Int,l:Int,h:Int):Int={

    
    var sum=0
    var cmp=0
    for(i<-centerx-l/2 to centerx+l/2){
      for(j<-centery-h/2 to centery+h/2){
        sum=sum+src(i)(j)
        //println("i : "+i+" j : "+j+" "+src(i)(j))
      cmp=cmp+1
    }
  }
  //println(cmp)
    return (sum/(cmp)).toInt
  }
  
  
  def variance(src:Array[Array[Int]],centerx:Int,centery:Int,l:Int,h:Int):Int={
    var cmp=0
    var sum:Double=0
    var moy=moyenne(src,centerx,centery,l,h)
    
     for(i<-centerx-l/2 to centerx+l/2){
        for(j<-centery-h/2 to centery+h/2){
          sum=sum+(src(i)(j)*src(i)(j))
          cmp=cmp+1
        }
      }
    sum=(sum/cmp)-(moy*moy)
    
    return sum.toInt
    
  }
  
  def longueur(ax:Int,ay:Int,bx:Int,by:Int,sqrt:Int):Double={
    var dx=Math.abs(ax-bx)
    var dy=Math.abs(ay-by)
    
    if(sqrt==1)
      return Math.sqrt(dx*dx+dy*dy)
    else
      return dx*dx+dy*dy
  
}
  
  
  def angle3pt(ax:Int,ay:Int,bx:Int,by:Int,cx:Int,cy:Int):Double={
    var cosabc=(longueur(bx,by,cx,cy,0)+longueur(ax,ay,bx,by,0)-longueur(ax,ay,cx,cy,0))/(2*longueur(bx,by,cx,cy,1)*longueur(ax,ay,bx,by,1))
    return (Math.toDegrees(Math.acos(cosabc)))
  }
  
  
    def angle2pt(ax:Int,ay:Int,bx:Int,by:Int):Int={
    var dx=Math.abs(ay-by)
    var longab=longueur(ax,ay,bx,by,1)
    var cosabc=dx/longab
    return (Math.toDegrees(Math.acos(cosabc)).toInt)
  }
    
    
    
    
    
    
    def candidat(ax:Int,ay:Int,long:Int,rotation:Double,x0:Int,y0:Int):Array[Array[Int]]={
        var candidatx=new Array[Double](9)
        var candidateqx=new Array[Double](16)
        var candidateqy=new Array[Double](16)
        var candidat2eqx=new Array[Int](16)
        var candidat2eqy=new Array[Int](16)
        var candidaty=new Array[Double](9)
        var candidat2x=new Array[Int](9)
        var candidat2y=new Array[Int](9)
        var rot=new Array[Double](18)
        var candidatmp=new Array[Double](9)
      
        candidaty(0)=0
        candidaty(1)=Math.cos(Math.toRadians(60))*long
        candidaty(2)=Math.cos(Math.toRadians(45))*long
        candidaty(3)=Math.cos(Math.toRadians(30))*long
        candidaty(4)=long
        candidaty(5)=Math.cos(Math.toRadians(30))*long
        candidaty(6)=Math.cos(Math.toRadians(45))*long
        candidaty(7)=Math.cos(Math.toRadians(60))*long
        candidaty(8)=0
        
        candidatx(0)=(-long)
        candidatx(1)=(-Math.sin(Math.toRadians(60)))*long
        candidatx(2)=(-Math.sin(Math.toRadians(45)))*long
        candidatx(3)=(-Math.sin(Math.toRadians(30)))*long
        candidatx(4)=0
        candidatx(5)=Math.sin(Math.toRadians(30))*long
        candidatx(6)=Math.sin(Math.toRadians(45))*long
        candidatx(7)=Math.sin(Math.toRadians(60))*long
        candidatx(8)=long
        
        
        
        
        if(y0<ay){
          if(x0<ax){
                    if(rotation>0 && rotation <90){
          for(i<-0 to 8){
            candidatmp(i)=candidaty(i)
            candidaty(i)=Math.cos(Math.toRadians(rotation))*candidaty(i)-Math.sin(Math.toRadians(rotation))*candidatx(i)
            candidatx(i)=Math.sin(Math.toRadians(rotation))*candidatmp(i)+Math.cos(Math.toRadians(rotation))*candidatx(i)
          }
        
        }
            
          }
          else if(x0>ax){
                    if(rotation>0 && rotation <90){
          for(i<-0 to 8){
            candidatmp(i)=candidaty(i)
            candidaty(i)=Math.cos(Math.toRadians(rotation))*candidaty(i)+(Math.sin(Math.toRadians(rotation))*candidatx(i))
            candidatx(i)=(-Math.sin(Math.toRadians(rotation)))*candidatmp(i)+Math.cos(Math.toRadians(rotation))*candidatx(i)
          }
        
        }
            
          }
        }
        else if(y0>ay){
          
          if(x0<ax){
                        for(i<-0 to 8){
            
            candidaty(i)=(-candidaty(i))
            candidatx(i)=(-candidatx(i))
          }
                                            if(rotation>0 && rotation <90){
          for(i<-0 to 8){
            candidatmp(i)=candidaty(i)
            candidaty(i)=Math.cos(Math.toRadians(rotation))*candidaty(i)+(Math.sin(Math.toRadians(rotation)))*candidatx(i)
            candidatx(i)=(-Math.sin(Math.toRadians(rotation)))*candidatmp(i)+Math.cos(Math.toRadians(rotation))*candidatx(i)
          }
        
        }
          }
          if(x0>ax){
                                    for(i<-0 to 8){
            
            candidaty(i)=(-candidaty(i))
            candidatx(i)=(-candidatx(i))
          }
                                            if(rotation>0 && rotation <90){
          for(i<-0 to 8){
            candidatmp(i)=candidaty(i)
            candidaty(i)=Math.cos(Math.toRadians(rotation))*candidaty(i)-(Math.sin(Math.toRadians(rotation))*candidatx(i))
            candidatx(i)=Math.sin(Math.toRadians(rotation))*candidatmp(i)+Math.cos(Math.toRadians(rotation))*candidatx(i)
          }
        
        }
            
          }
          
        }
        
        
        if(y0==ay){
          if(x0<ax){
            for(i<-0 to 8){
            candidatmp(i)=candidaty(i)
            candidaty(i)=(-candidatx(i))
            candidatx(i)=(candidatmp(i))
          }
          }
          if(x0>ax){
            for(i<-0 to 8){
            candidatmp(i)=candidaty(i)
            candidaty(i)=(candidatx(i))
            candidatx(i)=(-candidatmp(i))
          }
          }
        }
        
          if(x0==ax){
          
          if(y0>ay){
            
            for(i<-0 to 8){
            
            candidaty(i)=(-candidaty(i))
            candidatx(i)=(-candidatx(i))
          }
          
          }
        }
          
          
          if(x0==ax && y0==ay){
        candidateqy(0)=0
        candidateqy(1)=Math.cos(Math.toRadians(60))*long
        candidateqy(2)=Math.cos(Math.toRadians(45))*long
        candidateqy(3)=Math.cos(Math.toRadians(30))*long
        candidateqy(4)=long
        candidateqy(5)=Math.cos(Math.toRadians(30))*long
        candidateqy(6)=Math.cos(Math.toRadians(45))*long
        candidateqy(7)=Math.cos(Math.toRadians(60))*long
        candidateqy(8)=0
        
        candidateqy(9)=(-Math.cos(Math.toRadians(60)))*long
        candidateqy(10)=(-Math.cos(Math.toRadians(45)))*long
        candidateqy(11)=(-Math.cos(Math.toRadians(30)))*long
        candidateqy(12)=(-long)
        candidateqy(13)=(-Math.cos(Math.toRadians(30)))*long
        candidateqy(14)=(-Math.cos(Math.toRadians(45)))*long
        candidateqy(15)=(-Math.cos(Math.toRadians(60)))*long
        
        candidateqx(0)=(-long)
        candidateqx(1)=(-Math.sin(Math.toRadians(60)))*long
        candidateqx(2)=(-Math.sin(Math.toRadians(45)))*long
        candidateqx(3)=(-Math.sin(Math.toRadians(30)))*long
        candidateqx(4)=0
        candidateqx(5)=Math.sin(Math.toRadians(30))*long
        candidateqx(6)=Math.sin(Math.toRadians(45))*long
        candidateqx(7)=Math.sin(Math.toRadians(60))*long
        candidateqx(8)=long
        
        
        
        candidateqx(9)=(-Math.sin(Math.toRadians(60)))*long
        candidateqx(10)=(-Math.sin(Math.toRadians(45)))*long
        candidateqx(11)=(-Math.sin(Math.toRadians(30)))*long
        candidateqx(12)=0
        candidateqx(13)=Math.sin(Math.toRadians(30))*long
        candidateqx(14)=Math.sin(Math.toRadians(45))*long
        candidateqx(15)=Math.sin(Math.toRadians(60))*long
        
        for(i<-0 to 15){
          candidat2eqy(i)=candidateqy(i).toInt+ay
          candidat2eqx(i)=candidateqx(i).toInt+ax
        }
        
            return Array(candidat2eqx,candidat2eqy)
          }
        
        for(i<-0 to 8){
          candidat2y(i)=candidaty(i).toInt+ay
          candidat2x(i)=candidatx(i).toInt+ax
        }
        
        
        
        
        
        return Array(candidat2x,candidat2y)
      
    }
    
    
    
    
    def sortcandidat(ax:Int,ay:Int,candidat:Array[Array[Int]]):MutableList[Array[Int]]={
      var cdasize=candidat(0).length-1
      var initialvariance=moyenne(image2D,ax,ay,16,16)
      
      var tmpv=0
      var tmpx=0
      var tmpy=0
      var bestc:MutableList[Array[Int]]=MutableList()
      var tabvariance:Array[Int]=new Array[Int](cdasize+1)
      for(i<- 0 to cdasize){
        tabvariance(i)=moyenne(image2D,candidat(0)(i),candidat(1)(i),16,16)
        if(tabvariance(i)>=0.5*initialvariance){
          bestc+=Array(tabvariance(i),candidat(0)(i),candidat(1)(i))
        }
      }
        for(y<- 0 to bestc.length-1){     
         for(i<- 0 to bestc.length-2){
           if(bestc(i)(0)<=bestc(i+1)(0)){
             tmpv=bestc(i+1)(0)
             bestc(i+1)(0)=bestc(i)(0)
             bestc(i)(0)=tmpv
             
             tmpx=bestc(i+1)(1)
             bestc(i+1)(1)=bestc(i)(1)
             bestc(i)(1)=tmpx
             
             tmpy=bestc(i+1)(2)
             bestc(i+1)(2)=bestc(i)(2)
             bestc(i)(2)=tmpy
           } 
      
        }
        }
       return bestc
    }
    
    
    
val sobel=1
val bw=0
val moy=1
val grey=1
val laplace=0
val cmoy=20
val lpdiag="D"

if(grey==1)
  GreyLevel(image2D)
  

  
if(moy==1)
  moy(image2D,cmoy)

if(sobel==1)
  sobel(image2D)
    
if(laplace==1)
  laplace(image2D)
  
  
  
  var roads:MutableList[Array[Int]]=MutableList()
  
  def detectAnotherrecur(ax:Int,ay:Int):Boolean={
		
	for(i<-0 to roads.length-1){
	
		if(ax<=roads(i)(0)+8 && ax>=roads(i)(0)-8 && ay<=roads(i)(1)+8 && ay>=roads(i)(1)-8)
			return true
		else
			return false
	}
	return false
}
  
  
  def detectroad(x1s:Int,y1s:Int,x0s:Int,y0s:Int,cmp:Int){
   
    var x1=x1s 
    var y1=y1s
    var x0=x0s
    var y0=y0s
    var cda=candidat(x1,y1,15,angle2pt(x1,y1,x0,y0),x0,y0)
    var bestcd=sortcandidat(x0,y0,cda)

    if(roads.length!=1){    
    if((detectAnotherrecur(x1,y1)))
	    return false
    }
    if(cmp==0)
      return false
    
    if(x1>height-40 || x1<40 || y1>width-40 || y1<40)
      return false
    
    if(bestcd.length==0)
      return false
    
    else{
      
      if(roads.length==1)
      {
        for(i <- 0 to bestcd.length-1){
      x0=x1
      y0=y1
      x1=bestcd(i)(1)
      y1=bestcd(i)(2)
      roads+=(Array(x1,y1))
      detectroad(x1,y1,x0,y0,cmp-1) 
      }
      }
      if(bestcd.length>=2 ){
       //if(angle3pt(bestcd(0)(1),bestcd(0)(2),x1,y1,bestcd(1)(1),bestcd(1)(2))>=45){
      if(longueur(bestcd(0)(1),bestcd(0)(2),bestcd(1)(1),bestcd(1)(2),1)>15 && angle3pt(bestcd(0)(1),bestcd(0)(2),x1,y1,bestcd(1)(1),bestcd(1)(2))>=40){
      
      for(i <- 0 to 1){
      x0=x1
      y0=y1
      x1=bestcd(i)(1)
      y1=bestcd(i)(2)
      roads+=(Array(x1,y1))
      
      //println("recur"+cmp)
      detectroad(x1,y1,x0,y0,cmp-1)

      }
    
      }
       }
      
      
      x0=x1
      y0=y1
      x1=bestcd(0)(1)
      y1=bestcd(0)(2)
      roads+=(Array(x1,y1))
      
      //println("recur"+cmp)
      detectroad(x1,y1,x0,y0,cmp-1)
      
      }
    
  
  
  
}


  var max:Double=0
  var a=0
  var b=0
  
   var height=image2D.length;
 var width=image2D(0).length; 
 for(i<-30 to height-30){
   for(j<-30 to width-30){
  
  if(moyenne(image2D,i,j,16,16)>=max){
    max=moyenne(image2D,i,j,16,16)
    a=i
    b=j
  }
   }}
 
 

//



var a2=a+20
var b2=b-15


roads+=Array(a,b)
//roads+=(Array(a2,b2))


println("a : "+a+" b : "+b+" "+moyenne(image2D,a,b,10,10))
//println("a2 : "+a2+" b2 : "+b2+" "+moyenne(image2D,a2,b2,10,10))

detectroad(a,b,a,b,100)

println("recur exit")
/*for(j<- b-2 to b+2){

for(i<-a-2 to a+2){
       
          image2D(i)(j)=255
       }
        }

for(j<- b2-2 to b2+2){
for(i<-a2-2 to a2+2){
       
          image2D(i)(j)=255
       }
        }*/


/*
for(i<-a2-3 to a2+3){
       for(j<- b2-3 to b2+3){
          image2D(i)(j)=255
       }
        }*/



//var u=candidat(roads(3)(0),roads(3)(1),50,angle2pt(roads(3)(0),roads(3)(1),roads(2)(0),roads(2)(1)),roads(2)(0),roads(2)(1))

 /* for(y<-0 to u(0).length-1){
  println("x : "+u(0)(y)+" y : "+u(1)(y)+" moyenne "+moyenne(image2D,u(0)(y),u(1)(y),10,10))
}*/





//roads+=Array((getbestcandidat(a2,b2,u))(0),(getbestcandidat(a2,b2,u))(1))


//println((getbestcandidat(a2,b2,u))(0)+" "+(getbestcandidat(a2,b2,u))(1))







//var u=candidat(a,b,15,angle2pt(a,b,a,b),a,b)
//affiche2D(u)

  /*for(y<-0 to u(0).length-1){
  println("x : "+u(0)(y)+" y : "+u(1)(y)+" moy "+moyenne(image2D,u(0)(y),u(1)(y),16,16))
}

var t=sortcandidat(a,b,u)
  for(y<-0 to t.length-1){
  println("Sorted x : "+t(y)(1)+" y : "+t(y)(2)+" moy "+t(y)(0))
}
*/

for(i<- 0 to image2D.length-1){
  for(j<- 0 to image2D(0).length-1){
    image2D(i)(j)=save2d(i)(j)
  }
}



println(roads.length)
for(y<-0 to roads.size-1){
     for(i<- ((roads(y))(0)-3) to roads(y)(0)+3){
       for(j<- ((roads(y))(1)-3) to roads(y)(1)+3){
          image2D(i)(j)=255
       }
        }
      }


/*for(y<-0 to u(0).size-1){
     for(i<- (u(0)(y)-2) to u(0)(y)+2){
       for(j<- (u(1)(y)-2) to u(1)(y)+2){
          image2D(i)(j)=255
          
          
       }
        }
      }*/

var outputFile : String = chemin+num+"grey-"+grey+"_moy-"+moy+"c"+cmoy+"_sobel-"+sobel+"_laplace-"+laplace+lpdiag+"_"+file;
 
 println("fin")
wrappedImage.saveImage(outputFile);
  
}